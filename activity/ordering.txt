===ORDER===
{
    userId: ObjectId,
    transactionDate: Date,
    status: String,
    total: Number
}

===PRODUCTS===
{
    productId:ObjectId,
    name:String,
    description: String,
    price: Number,
    stocks: Number,
    isActive: Boolean,
    SKU: Number
}

===USERS===
 {
    userId: String,
    firstName: String,
    lastName: String,
    email: String,
    isAdmin: Boolean,
    mobileNumber: Number
}

===orderProducts===
{
    orderId: String,
    productId: String,
    quantity: Number,
    price: Number,
    subtotal: Number
}